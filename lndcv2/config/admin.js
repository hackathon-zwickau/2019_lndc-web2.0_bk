ADMIN_EMAIL = process.env.ADMIN_EMAIL;
ADMIN_PASSWORD = process.env.ADMIN_PASS;

let User = require('../app/server/models/User');

User
  .findOne({
    email: ADMIN_EMAIL
  })
  .exec(function(err, user){
    if (!user){
      let u = new User();
      u.email = ADMIN_EMAIL;
      u.password = User.generateHash(ADMIN_PASSWORD);
      u.admin = true;
      u.verified = true;
      u.save(function(err){
        if (err){
          console.log(err);
        }
      });
    }
  });
