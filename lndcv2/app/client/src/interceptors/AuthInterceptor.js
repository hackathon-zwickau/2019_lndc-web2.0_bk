angular.module('reg').factory('AuthInterceptor', [
    'Session',
    (Session) => {
        return {
            request: (config) => {
                let token = Session.getToken();
                if (token) config.headers['x-access-token'] = token;
                return config;
            }
        };
    }]);