let angular = require('angular');
angular.module('reg').factory('AuthService', [
    '$http',
    '$rootScope',
    '$state',
    '$window',
    'Session',
    ($http, $rootScope, $state, $window, Session) => {
        let authService = {},
            loginSuccess = (data, cb) => {
                Session.create(data.token, data.user);

                if (cb) {
                    cb(data.user);
                }
            },
            loginFailure = (data, cb) => {
                $state.go('login');
                if (cb) {
                    cb(data);
                }
            };
        authService.loginWithPassword = (email, password, onSuccess, onFailure) => {
            return $http.post('/auth/login', {
                email: email,
                password: password
            }).then(response => {
                loginSuccess(response.data, onSuccess);
            }, response => {
                loginFailure(response.data, onFailure);
            });
        };

        authService.loginWithToken = (token, onSuccess, onFailure) => {
            return $http.post('/auth/login', {
                token: token
            }).then(response => {
                loginSuccess(response.data, onSuccess);
            }, response => {
                if (response.status === 400) {
                    Session.destroy(loginFailure);
                }
            });
        };
        authService.logout = (callback) => {
            Session.destroy(callback);
            $state.go('login');
        };

        authService.register = (email, password, onSuccess, onFailure) => {
            return $http.post('/auth/register', {
                email: email,
                password: password
            }).then(response => {
                loginSuccess(response.data, onSuccess);
            }, response => {
                loginFailure(response.data, onFailure);
            });
        };

        authService.verify = (token, onSuccess, onFailure) => {
            return $http.get('/auth/verify/' + token).then(response => {
                Session.setUser(response.data);
                if (onSuccess) onSuccess(response.data);
            }, response => {
                if (onFailure) onFailure(response.data);
            });
        };
        authService.resendVerificationEmail = (onSuccess, onFailure) => {
            return $http.post('/auth/verify/resend', {
                id: Session.getUserId()
            });
        };
        authService.sendResetEmail = (email) => {
            return $http.post('/auth/reset', {
                email: email
            });
        };
        authService.resetPassword = (token, pass, onSuccess, onFailure) => {
            return $http.post('/auth/reset/password', {
                token: token,
                password: pass
            }).then(onSuccess, onFailure);
        };
        return authService;
    }
]);
