angular.module('reg')
    .factory('UserService', [
        '$http',
        'Session',
        ($http, Session) => {
            let users = '/api/users', base = users + '/';
            return {
                getCurrentUser: () => {
                    return $http.get(base + Session.getUserId());
                },
                get: (id) => {
                    return $http.get(base + id);
                },
                getAll: () => {
                    return $http.get(base);
                },
                getPage: (page, size, text) => {
                    return $http.get(users + '?' + $.param(
                        {
                            text: text,
                            page: page ? page : 0,
                            size: size ? size : 50
                        })
                    );
                },
                updateProfile: (id, profile) => {
                    return $http.put(base + id + '/profile', {
                        profile: profile
                    });
                },

                updateConfirmation: (id, confirmation) => {
                    return $http.put(base + id + '/confirm', {
                        confirmation: confirmation
                    });
                },

                declineAdmission: (id) => {
                    return $http.post(base + id + '/decline');
                },
                joinOrCreateTeam: (code) => {
                    return $http.put(base + Session.getUserId() + '/team', {
                        code: code
                    });
                },

                leaveTeam: () => {
                    return $http.delete(base + Session.getUserId() + '/team');
                },

                getMyTeammates: () => {
                    return $http.get(base + Session.getUserId() + '/team');
                },
                getStats: () => {
                    return $http.get(base + 'stats');
                },

                admitUser: (id) => {
                    return $http.post(base + id + '/admit');
                },

                checkIn: (id) => {
                    return $http.post(base + id + '/checkin');
                },

                checkOut: (id) => {
                    return $http.post(base + id + '/checkout');
                },

                makeAdmin: (id) => {
                    return $http.post(base + id + '/makeadmin');
                },

                removeAdmin: (id) => {
                    return $http.post(base + id + '/removeadmin');
                },
            };
        }
    ]);
