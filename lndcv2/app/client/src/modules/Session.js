angular.module('reg').service('Session', [
    '$rootScope',
    '$window',
    ($rootScope, $window) => {
        this.create = (token, user) => {
            $window.localStorage.jwt = token;
            $window.localStorage.userId = user._id;
            $window.localStorage.currentUser = JSON.stringify(user);
            $rootScope.currentUser = user;
        };
        this.destroy = (onComplete) => {
            delete $window.localStorage.jwt;
            delete $window.localStorage.userId;
            delete $window.localStorage.currentUser;
            $rootScope.currentUser = null;
            if (onComplete) onComplete();
        };
        this.getToken = () => {
            return $window.localStorage.jwt;
        };
        this.getUserId = () => {
            return $window.localStorage.userId;
        };
        this.getUser = () => {
            return JSON.parse($window.localStorage.currentUser);
        };
        this.setUser = (user) => {
            $window.localStorage.currentUser = JSON.stringify(user);
            $rootScope.currentUser = user;
        };
    }]);