const angular = require('angular'), moment = require('moment');
angular.module('reg').factory('Utils', [
    () => {
        return {
            isRegOpen: (settings) => {
                return Date.now() > settings.timeOpen && Date.now() < settings.timeClose;
            },
            isAfter: (time) => {
                return Date.now() > time;
            },
            formatTime: (time) => {
                if (!time) return "Invalid Date";
                date = new Date(time);
                return moment(date).format('dddd, MMMM Do YYYY, h:mm a') + " " + date.toTimeString().split(' ')[2];
            }
        };
    }]);
