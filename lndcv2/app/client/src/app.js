const $ = require('jquery'),
    constants = require('./constants.js');

let angular = require('angular'),
    uiRouter = require('angular-ui-router'),
    app = angular.module('reg', [
        'ui.router',
    ]), AuthService = require('./services/AuthService.js'),
    AuthInterceptor = require('./interceptors/AuthInterceptor.js'),
    Session = require('./modules/Session.js'),
    routes = require('./routes.js');
app.config([
    '$httpProvider',
    ($httpProvider) => {
        $httpProvider.interceptors.push('AuthInterceptor');

    }]).run([
    'AuthService',
    'Session',
    (AuthService, Session) => {
        let token = Session.getToken();
        if (token) AuthService.loginWithToken(token);
    }]);