const angular = require('angular');
const Utils = require('../../src/modules/Utils.js');

angular.module('reg')
    .service('settings',  () =>{
    })
    .controller('SidebarCtrl', [
        '$rootScope',
        '$scope',
        'settings',
        'Utils',
        'AuthService',
        'Session',
        'EVENT_INFO',
         ($rootScope, $scope, settings, Utils, AuthService, Session, EVENT_INFO) =>{

            //let settings = settings.data;
            let user = $rootScope.currentUser;

            $scope.EVENT_INFO = EVENT_INFO;

            $scope.pastConfirmation = Utils.isAfter(user.status.confirmBy);

            $scope.logout =  () =>{
                AuthService.logout();
            };

            $scope.showSidebar = false;
            $scope.toggleSidebar =  () =>{
                $scope.showSidebar = !$scope.showSidebar;
            };

            // oh god jQuery hack
            $('.item').on('click',  ()=> {
                $scope.showSidebar = false;
            });

        }]);
