const angular = require("angular"),
    swal = require("sweetalert");

angular.module('reg')
    .controller('ApplicationCtrl', [
        '$scope',
        '$rootScope',
        '$state',
        '$http',
        'currentUser',
        'settings',
        'Session',
        'UserService',
        ($scope, $rootScope, $state, $http, currentUser, settings, Session, UserService) => {
            $scope.user = currentUser.data;
            $scope.isMitStudent = $scope.user.email.split('@')[1] === 'fh-zwickau.de';
            if ($scope.isMitStudent) $scope.user.profile.adult = true;
            populateSchools();
            _setupForm();
            $scope.regIsClosed = Date.now() > settings.data.timeClose;

            function populateSchools() {
                $http
                    .get('/assets/schools.json')
                    .then((res) => {
                        let schools = res.data,
                            email = $scope.user.email.split('@')[1];
                        if (schools[email]) {
                            $scope.user.profile.school = schools[email].school;
                            $scope.autoFilledSchool = true;
                        }
                    });

                $http
                    .get('/assets/schools.csv').then((res) => {
                    $scope.schools = res.data.split('\n');
                    $scope.schools.push('Other');
                    let content = [];
                    for (i = 0; i < $scope.schools.length; i++) {
                        $scope.schools[i] = $scope.schools[i].trim();
                        content.push({title: $scope.schools[i]})
                    }
                    $('#school.ui.search')
                        .search({
                            source: content,
                            cache: true,
                            onSelect: (result, response) => {
                                $scope.user.profile.school = result.title.trim();
                            }
                        })
                });
            }

            function _updateUser(e) {
                UserService.updateProfile(Session.getUserId(), $scope.user.profile).then(response => {
                    swal("Awesome!", "Your application has been saved.", "success").then(value => {
                        $state.go("app.dashboard");
                    });
                }, response => {
                    swal("Uh oh!", "Something went wrong.", "error");
                });
            }

            function isMinor() {
                return !$scope.user.profile.adult;
            }

            function minorsAreAllowed() {
                return settings.data.allowMinors;
            }

            function minorsValidation() {
                return !(isMinor() && !minorsAreAllowed());
            }

            function _setupForm() {
                $.fn.form.settings.rules.allowMinors = (value) => {
                    return minorsValidation();
                };
                $('.ui.form').form({
                    inline: true,
                    fields: {
                        name: {
                            identifier: 'name',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please enter your name.'
                                }
                            ]
                        },
                        school: {
                            identifier: 'school',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please enter your school or university name.'
                                }
                            ]
                        },
                        year: {
                            identifier: 'year',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please select your graduation year.'
                                }
                            ]
                        },
                        gender: {
                            identifier: 'gender',
                            rules: [
                                {
                                    type: 'empty',
                                    prompt: 'Please select a gender.'
                                }
                            ]
                        },
                        adult: {
                            identifier: 'adult',
                            rules: [
                                {
                                    type: 'allowMinors',
                                    prompt: 'You must be an adult, or an WHZ student.'
                                }
                            ]
                        }
                    }
                });
            }

            $scope.submitForm = ()=> {
                if ($('.ui.form').form('is valid')) _updateUser(); else swal("Uh oh!", "Please Fill The Required Fields", "error");
            };
        }]);