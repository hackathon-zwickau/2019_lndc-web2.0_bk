angular.module('reg')
    .controller('VerifyCtrl', [
        '$scope',
        '$stateParams',
        'AuthService',
        function ($scope, $stateParams, AuthService) {
            let token = $stateParams.token;
            $scope.loading = true;
            if (token) {
                AuthService.verify(token,
                    (user) => {
                        $scope.success = true;
                        $scope.loading = false;
                    },
                    (err) => {
                        $scope.loading = false;
                    });
            }
        }]);
