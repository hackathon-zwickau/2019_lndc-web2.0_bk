const swal = require('sweetalert');
angular.module('reg')
    .controller('ResetCtrl', [
        '$scope',
        '$stateParams',
        '$state',
        'AuthService',
        ($scope, $stateParams, $state, AuthService) => {
            let token = $stateParams.token;
            $scope.loading = true;
            $scope.changePassword = ()=> {
                let password = $scope.password,
                    confirm = $scope.confirm;
                if (password !== confirm) {
                    $scope.error = "Passwords don't match!";
                    $scope.confirm = "";
                    return;
                }
                AuthService.resetPassword(
                    token,
                    $scope.password,
                    message => {
                        swal("Neato!", "Your password has been changed!", "success").then(value => {
                            $state.go("login");
                        });
                    },
                    data => {
                        $scope.error = data.message;
                        $scope.loading = false;
                    });
            };
        }]);
