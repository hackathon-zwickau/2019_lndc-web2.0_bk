const swal = require('sweetalert');
angular.module('reg')
    .controller('AdminUserCtrl', [
        '$scope',
        '$http',
        'user',
        'UserService',
        ($scope, $http, User, UserService) => {
            $scope.selectedUser = User.data;
            populateSchools();

            function populateSchools() {
                $http.get('/assets/schools.json').then(function (res) {
                    let schools = res.data,
                        email = $scope.selectedUser.email.split('@')[1];
                    if (schools[email]) {
                        $scope.selectedUser.profile.school = schools[email].school;
                        $scope.autoFilledSchool = true;
                    }
                });
            }

            $scope.updateProfile = () => {
                UserService.updateProfile($scope.selectedUser._id, $scope.selectedUser.profile).then(response => {
                    $selectedUser = response.data;
                    swal("Updated!", "Profile updated.", "success");
                }, response => {
                    swal("Oops, you forgot something.");
                });
            };
        }]);