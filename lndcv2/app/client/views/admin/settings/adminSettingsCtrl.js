const moment = require('moment'),
    showdown = require('showdown'),
    swal = require('sweetalert');

angular.module('reg')
    .controller('AdminSettingsCtrl', [
        '$scope',
        '$sce',
        'SettingsService',
        ($scope, $sce, SettingsService) => {

            $scope.settings = {};
            SettingsService.getPublicSettings().then(response => {
                updateSettings(response.data);
            });

            function updateSettings(settings) {
                $scope.loading = false;
                settings.timeOpen = new Date(settings.timeOpen);
                settings.timeClose = new Date(settings.timeClose);
                settings.timeConfirm = new Date(settings.timeConfirm);
                $scope.settings = settings;
            }

            $scope.updateAllowMinors = () => {
                SettingsService.updateAllowMinors($scope.settings.allowMinors).then(response => {
                    $scope.settings.allowMinors = response.data.allowMinors;
                    const successText = $scope.settings.allowMinors ?
                        "Minors are now allowed to register." :
                        "Minors are no longer allowed to register."
                    swal("Looks good!", successText, "success");
                });
            };

            SettingsService.getWhitelistedEmails().then(response => {
                $scope.whitelist = response.data.join(", ");
            });

            $scope.updateWhitelist = () => {
                SettingsService.updateWhitelistedEmails($scope.whitelist.replace(/ /g, '').split(','))
                    .then(response => {
                        swal('Whitelist updated.');
                        $scope.whitelist = response.data.whitelistedEmails.join(", ");
                    });
            };
            $scope.formatDate = function (date) {
                if (!date) return "Invalid Date";
                return moment(date).format('dddd, MMMM Do YYYY, h:mm a') + " " + date.toTimeString().split(' ')[2];
            };

            function cleanDate(date) {
                return new Date(
                    date.getFullYear(),
                    date.getMonth(),
                    date.getDate(),
                    date.getHours(),
                    date.getMinutes()
                );
            }

            $scope.updateRegistrationTimes = () => {
                let open = cleanDate($scope.settings.timeOpen).getTime(),
                    close = cleanDate($scope.settings.timeClose).getTime();

                if (open < 0 || close < 0 || open === undefined || close === undefined)
                    return swal('Oops...', 'You need to enter valid times.', 'error');

                if (open >= close) {
                    swal('Oops...', 'Registration cannot open after it closes.', 'error');
                    return;
                }

                SettingsService.updateRegistrationTimes(open, close).then(response => {
                    updateSettings(response.data);
                    swal("Looks good!", "Registration Times Updated", "success");
                });
            };

            $scope.updateConfirmationTime = () => {
                let confirmBy = cleanDate($scope.settings.timeConfirm).getTime();

                SettingsService.updateConfirmationTime(confirmBy).then(response => {
                    updateSettings(response.data);
                    swal("Sounds good!", "Confirmation Date Updated", "success");
                });
            };

            let converter = new showdown.Converter();

            $scope.markdownPreview = (text) => {
                return $sce.trustAsHtml(converter.makeHtml(text));
            };

            $scope.updateWaitlistText = () => {
                let text = $scope.settings.waitlistText;
                SettingsService.updateWaitlistText(text).then(response => {
                    swal("Looks good!", "Waitlist Text Updated", "success");
                    updateSettings(response.data);
                });
            };

            $scope.updateAcceptanceText = () => {
                let text = $scope.settings.acceptanceText;
                SettingsService.updateAcceptanceText(text).then(response => {
                    swal("Looks good!", "Acceptance Text Updated", "success");
                    updateSettings(response.data);
                });
            };

            $scope.updateConfirmationText = () => {
                let text = $scope.settings.confirmationText;
                SettingsService.updateConfirmationText(text).then(response => {
                    swal("Looks good!", "Confirmation Text Updated", "success");
                    updateSettings(response.data);
                });
            };
        }]);