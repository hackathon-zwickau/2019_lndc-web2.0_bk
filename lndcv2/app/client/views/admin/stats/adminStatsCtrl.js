const moment = require('moment');

angular.module('reg')
    .controller('AdminStatsCtrl', [
        '$scope',
        'UserService',
        ($scope, UserService) => {
            UserService.getStats().then(stats => {
                $scope.stats = stats.data;
                $scope.loading = false;
            });

            $scope.fromNow = (date) => {
                return moment(date).fromNow();
            };

        }]);
