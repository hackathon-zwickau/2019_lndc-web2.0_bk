let _         = require('underscore'),
    SettingsController = require('../controllers/SettingsController'),
    UserController = require('../controllers/UserController');

module.exports = function(router){

  router.post('/login',
    (req, res, next)=>{
      let email = req.body.email,
          password = req.body.password,
          token = req.body.token;

      if (token) {
        UserController.loginWithToken(token,
          (err, token, user)=>{
            if (err || !user) {
              return res.status(400).send(err);
            }
            return res.json({
              token: token,
              user: user
            });
          });
      } else {
        UserController.loginWithPassword(email, password,
          (err, token, user)=>{
            if (err || !user) {
              return res.status(400).send(err);
            }
            return res.json({
              token: token,
              user: user
            });
          });
      }
  });

  router.post('/register',
    (req, res, next)=>{
      let email = req.body.email,
          password = req.body.password;
      UserController.createUser(email, password,
        (err, user)=>{
          if (err) return res.status(400).send(err);
          return res.json(user);
      });
  });

  router.post('/reset',
    (req, res, next)=>{
      let email = req.body.email;
      if (!email) return res.status(400).send();
      UserController.sendPasswordResetEmail(email, (err)=>{
        if(err) return res.status(400).send(err);
        return res.json({message: 'Email Sent'});
      });
  });

  router.post('/reset/password', (req, res)=>{
    let pass = req.body.password,
        token = req.body.token;
    UserController.resetPassword(token, pass, (err, user)=>{
      if (err || !user) return res.status(400).send(err);
      return res.json(user);
    });
  });

  router.post('/verify/resend',
    (req, res, next)=>{
      let id = req.body.id;
      if (id){
        UserController.sendVerificationEmailById(id, (err, user)=>{
          if (err || !user) return res.status(400).send();
          return res.status(200).send();
        });
      } else return res.status(400).send();
  });

   router.get('/verify/:token',
    (req, res, next)=>{
      let token = req.params.token;
      UserController.verifyByToken(token, (err, user)=>{
        if (err || !user) return res.status(400).send(err);
        return res.json(user);
      });
    });
};
