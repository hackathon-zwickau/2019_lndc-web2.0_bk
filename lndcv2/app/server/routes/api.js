let UserController = require('../controllers/UserController'),
    SettingsController = require('../controllers/SettingsController'),
    request = require('request');

module.exports =  (router)=> {

    function getToken(req) {
        return req.headers['x-access-token'];
    }

    function isAdmin(req, res, next) {

        let token = getToken(req);

        UserController.getByToken(token, (err, user)=> {

            if (err)
                return res.status(500).send(err);

            if (user && user.admin) {
                req.user = user;
                return next();
            }

            return res.status(401).send({
                message: 'Get outta here, punk!'
            });

        });
    }

    function isOwnerOrAdmin(req, res, next) {
        let token = getToken(req);
        let userId = req.params.id;

        UserController.getByToken(token,  (err, user)=> {

            if (err || !user) {
                return res.status(500).send(err);
            }

            if (user._id === userId || user.admin) {
                return next();
            }
            return res.status(400).send({
                message: 'Token does not match user id.'
            });
        });
    }
    function defaultResponse(req, res) {
        return  (err, data) =>{
            if (err) {
                if (process.env.NODE_ENV === 'production') {
                    request
                        .post(process.env.SLACK_HOOK,
                            {
                                form: {
                                    payload: JSON.stringify({
                                        "text":
                                            "``` \n" +
                                            "Request: \n " +
                                            req.method + ' ' + req.url +
                                            "\n ------------------------------------ \n" +
                                            "Body: \n " +
                                            JSON.stringify(req.body, null, 2) +
                                            "\n ------------------------------------ \n" +
                                            "\nError:\n" +
                                            JSON.stringify(err, null, 2) +
                                            "``` \n"
                                    })
                                }
                            },
                             (error, response, body) =>{
                                return res.status(500).send({
                                    message: "Your error has been recorded, we'll get right on it!"
                                });
                            }
                        );
                } else {
                    return res.status(500).send(err);
                }
            } else {
                return res.json(data);
            }
        };
    }

    router.get('/users', isAdmin, function (req, res) {
        let query = req.query;
        if (query.page && query.size)
            UserController.getPage(query, defaultResponse(req, res));
         else
            UserController.getAll(defaultResponse(req, res));
    });
    router.get('/users/stats', isAdmin, function (req, res) {
        UserController.getStats(defaultResponse(req, res));
    });

    router.get('/users/:id', isOwnerOrAdmin, function (req, res) {
        UserController.getById(req.params.id, defaultResponse(req, res));
    });

    router.put('/users/:id/profile', isOwnerOrAdmin, function (req, res) {
        let profile = req.body.profile,
            id = req.params.id;
        UserController.updateProfileById(id, profile, defaultResponse(req, res));
    });

    router.put('/users/:id/confirm', isOwnerOrAdmin, function (req, res) {
        let confirmation = req.body.confirmation,
            id = req.params.id;
        UserController.updateConfirmationById(id, confirmation, defaultResponse(req, res));
    });

    router.post('/users/:id/decline', isOwnerOrAdmin, function (req, res) {
        let confirmation = req.body.confirmation,
            id = req.params.id;
        UserController.declineById(id, defaultResponse(req, res));
    });

    router.get('/users/:id/team', isOwnerOrAdmin, function (req, res) {
        let id = req.params.id;
        UserController.getTeammates(id, defaultResponse(req, res));
    });

    router.put('/users/:id/team', isOwnerOrAdmin, function (req, res) {
        let code = req.body.code,
            id = req.params.id;
        UserController.createOrJoinTeam(id, code, defaultResponse(req, res));
    });

    router.delete('/users/:id/team', isOwnerOrAdmin, function (req, res) {
        UserController.leaveTeam(req.params.id, defaultResponse(req, res));
    });
    router.put('/users/:id/password', isOwnerOrAdmin, function (req, res) {
        return res.status(304).send();
    });

    router.post('/users/:id/admit', isAdmin, function (req, res) {
        let id = req.params.id,
            user = req.user;
        UserController.admitUser(id, user, defaultResponse(req, res));
    });

    router.post('/users/:id/checkin', isAdmin, function (req, res) {
        let id = req.params.id,
            user = req.user;
        UserController.checkInById(id, user, defaultResponse(req, res));
    });

    router.post('/users/:id/checkout', isAdmin, function (req, res) {
        let id = req.params.id,
            user = req.user;
        UserController.checkOutById(id, user, defaultResponse(req, res));
    });

    router.post('/users/:id/makeadmin', isAdmin, function (req, res) {
        let id = req.params.id,
            user = req.user;
        UserController.makeAdminById(id, user, defaultResponse(req, res));
    });

    router.post('/users/:id/removeadmin', isAdmin, function (req, res) {
        let id = req.params.id,
            user = req.user;
        UserController.removeAdminById(id, user, defaultResponse(req, res));
    });

    router.get('/settings', function (req, res) {
        SettingsController.getPublicSettings(defaultResponse(req, res));
    });

    router.put('/settings/waitlist', isAdmin, function (req, res) {
        let text = req.body.text;
        SettingsController.updateField('waitlistText', text, defaultResponse(req, res));
    });

    router.put('/settings/acceptance', isAdmin, function (req, res) {
        let text = req.body.text;
        SettingsController.updateField('acceptanceText', text, defaultResponse(req, res));
    });

    router.put('/settings/confirmation', isAdmin, function (req, res) {
        let text = req.body.text;
        SettingsController.updateField('confirmationText', text, defaultResponse(req, res));
    });

    router.put('/settings/confirm-by', isAdmin, function (req, res) {
        let time = req.body.time;
        SettingsController.updateField('timeConfirm', time, defaultResponse(req, res));
    });

    router.put('/settings/times', isAdmin, function (req, res) {
        let open = req.body.timeOpen,
            close = req.body.timeClose;
        SettingsController.updateRegistrationTimes(open, close, defaultResponse(req, res));
    });

    router.get('/settings/whitelist', isAdmin, function (req, res) {
        SettingsController.getWhitelistedEmails(defaultResponse(req, res));
    });

    router.put('/settings/whitelist', isAdmin, function (req, res) {
        let emails = req.body.emails;
        SettingsController.updateWhitelistedEmails(emails, defaultResponse(req, res));
    });

    router.put('/settings/minors', isAdmin, function (req, res) {
        let allowMinors = req.body.allowMinors;
        SettingsController.updateField('allowMinors', allowMinors, defaultResponse(req, res));
    });
};
