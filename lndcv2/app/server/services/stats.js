let _ = require('underscore'),
    async = require('async'),
    User = require('../models/User'),
    stats = {};

function calculateStats() {
    console.log('Calculating stats...');
    let newStats = {
        lastUpdated: 0,

        total: 0,
        demo: {
            gender: {
                M: 0,
                F: 0,
                O: 0,
                N: 0
            },
            schools: {},
            year: {
                '2000': 0,
                '2001': 0,
                '2002': 0,
                '2003': 0,
                '2004': 0,
                '2005': 0,
                '2006': 0,
                '2007': 0,
                '2008': 0,
                '2009': 0,
                '2010': 0,
                '2011': 0,
                '2012': 0,
                '2013': 0,
                '2014': 0,
                '2015': 0,
                '2016': 0,
                '2017': 0,
                '2018': 0,
                '2019': 0,
            }
        },

        teams: {},
        verified: 0,
        submitted: 0,
        admitted: 0,
        confirmed: 0,
        confirmedMit: 0,
        declined: 0,

        confirmedFemale: 0,
        confirmedMale: 0,
        confirmedOther: 0,
        confirmedNone: 0,

        shirtSizes: {
            'XS': 0,
            'S': 0,
            'M': 0,
            'L': 0,
            'XL': 0,
            'XXL': 0,
            'WXS': 0,
            'WS': 0,
            'WM': 0,
            'WL': 0,
            'WXL': 0,
            'WXXL': 0,
            'None': 0
        },

        dietaryRestrictions: {},

        hostNeededFri: 0,
        hostNeededSat: 0,
        hostNeededUnique: 0,

        hostNeededFemale: 0,
        hostNeededMale: 0,
        hostNeededOther: 0,
        hostNeededNone: 0,

        reimbursementTotal: 0,
        reimbursementMissing: 0,

        wantsHardware: 0,

        checkedIn: 0
    };

    User
        .find({})
        .exec(function (err, users) {
            if (err || !users) {
                throw err;
            }

            newStats.total = users.length;

            async.each(users, function (user, callback) {
                let email = user.email.split('@')[1];

                newStats.demo.gender[user.profile.gender] += 1;

                newStats.verified += user.verified ? 1 : 0;

                newStats.submitted += user.status.completedProfile ? 1 : 0;

                newStats.admitted += user.status.admitted ? 1 : 0;

                newStats.confirmed += user.status.confirmed ? 1 : 0;

                newStats.confirmedMit += user.status.confirmed && email === "fh-zwickau.de" ? 1 : 0;

                newStats.confirmedFemale += user.status.confirmed && user.profile.gender === "F" ? 1 : 0;
                newStats.confirmedMale += user.status.confirmed && user.profile.gender === "M" ? 1 : 0;
                newStats.confirmedOther += user.status.confirmed && user.profile.gender === "O" ? 1 : 0;
                newStats.confirmedNone += user.status.confirmed && user.profile.gender === "N" ? 1 : 0;

                newStats.declined += user.status.declined ? 1 : 0;

                newStats.reimbursementTotal += user.confirmation.needsReimbursement ? 1 : 0;

                newStats.reimbursementMissing += user.confirmation.needsReimbursement &&
                !user.status.reimbursementGiven ? 1 : 0;

                newStats.wantsHardware += user.confirmation.wantsHardware ? 1 : 0;

                if (!newStats.demo.schools[email]) {
                    newStats.demo.schools[email] = {
                        submitted: 0,
                        admitted: 0,
                        confirmed: 0,
                        declined: 0,
                    };
                }
                newStats.demo.schools[email].submitted += user.status.completedProfile ? 1 : 0;
                newStats.demo.schools[email].admitted += user.status.admitted ? 1 : 0;
                newStats.demo.schools[email].confirmed += user.status.confirmed ? 1 : 0;
                newStats.demo.schools[email].declined += user.status.declined ? 1 : 0;

                if (user.profile.graduationYear) {
                    newStats.demo.year[user.profile.graduationYear] += 1;
                }

                if (user.confirmation.shirtSize in newStats.shirtSizes) {
                    newStats.shirtSizes[user.confirmation.shirtSize] += 1;
                }

                newStats.hostNeededFri += user.confirmation.hostNeededFri ? 1 : 0;
                newStats.hostNeededSat += user.confirmation.hostNeededSat ? 1 : 0;
                newStats.hostNeededUnique += user.confirmation.hostNeededFri || user.confirmation.hostNeededSat ? 1 : 0;

                newStats.hostNeededFemale
                    += (user.confirmation.hostNeededFri || user.confirmation.hostNeededSat) && user.profile.gender === "F" ? 1 : 0;
                newStats.hostNeededMale
                    += (user.confirmation.hostNeededFri || user.confirmation.hostNeededSat) && user.profile.gender === "M" ? 1 : 0;
                newStats.hostNeededOther
                    += (user.confirmation.hostNeededFri || user.confirmation.hostNeededSat) && user.profile.gender === "O" ? 1 : 0;
                newStats.hostNeededNone
                    += (user.confirmation.hostNeededFri || user.confirmation.hostNeededSat) && user.profile.gender === "N" ? 1 : 0;

                if (user.confirmation.dietaryRestrictions) {
                    user.confirmation.dietaryRestrictions.forEach(function (restriction) {
                        if (!newStats.dietaryRestrictions[restriction]) {
                            newStats.dietaryRestrictions[restriction] = 0;
                        }
                        newStats.dietaryRestrictions[restriction] += 1;
                    });
                }

                newStats.checkedIn += user.status.checkedIn ? 1 : 0;

                callback();
            }, ()=> {
                let restrictions = [];
                _.keys(newStats.dietaryRestrictions)
                    .forEach(function (key) {
                        restrictions.push({
                            name: key,
                            count: newStats.dietaryRestrictions[key],
                        });
                    });
                newStats.dietaryRestrictions = restrictions;

                let schools = [];
                _.keys(newStats.demo.schools)
                    .forEach(function (key) {
                        schools.push({
                            email: key,
                            count: newStats.demo.schools[key].submitted,
                            stats: newStats.demo.schools[key]
                        });
                    });
                newStats.demo.schools = schools;

                console.log('Stats updated!');
                newStats.lastUpdated = new Date();
                stats = newStats;
            });
        });

}

calculateStats();
setInterval(calculateStats, 300000);

let Stats = {};

Stats.getUserStats = ()=> {
    return stats;
};

module.exports = Stats;
