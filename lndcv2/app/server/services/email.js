let path = require('path'),
    nodemailer = require('nodemailer'),
    smtpTransport = require('nodemailer-smtp-transport'),
    templatesDir = path.join(__dirname, '../templates'),
    Email = require('email-templates'),
    ROOT_URL = process.env.ROOT_URL,
    HACKATHON_NAME = process.env.HACKATHON_NAME,
    EMAIL_ADDRESS = process.env.EMAIL_ADDRESS,
    TWITTER_HANDLE = process.env.TWITTER_HANDLE,
    FACEBOOK_HANDLE = process.env.FACEBOOK_HANDLE,
    EMAIL_HOST = process.env.EMAIL_HOST,
    EMAIL_USER = process.env.EMAIL_USER,
    EMAIL_PASS = process.env.EMAIL_PASS,
    EMAIL_PORT = process.env.EMAIL_PORT,
    EMAIL_CONTACT = process.env.EMAIL_CONTACT,
    EMAIL_HEADER_IMAGE = process.env.EMAIL_HEADER_IMAGE;

if (EMAIL_HEADER_IMAGE.indexOf("https") === -1) {
    EMAIL_HEADER_IMAGE = ROOT_URL + EMAIL_HEADER_IMAGE;
}

let NODE_ENV = process.env.NODE_ENV,
    options = {
    host: EMAIL_HOST,
    port: EMAIL_PORT,
    secure: true,
    auth: {
        user: EMAIL_USER,
        pass: EMAIL_PASS
    }
},
    transporter = nodemailer.createTransport(smtpTransport(options)),
    controller = {};

controller.transporter = transporter;

function sendOne(templateName, options, data, callback) {
    if (NODE_ENV === "dev") {
        console.log(templateName);
        console.log(JSON.stringify(data, "", 2));
    }

    const email = new Email({
        message: {
            from: EMAIL_ADDRESS
        },
        send: true,
        transport: transporter
    });

    data.emailHeaderImage = EMAIL_HEADER_IMAGE;
    data.emailAddress = EMAIL_ADDRESS;
    data.hackathonName = HACKATHON_NAME;
    data.twitterHandle = TWITTER_HANDLE;
    data.facebookHandle = FACEBOOK_HANDLE;

    email.send({
        locals: data,
        message: {
            subject: options.subject,
            to: options.to
        },
        template: path.join(__dirname, "..", "emails", templateName),
    }).then(res => {
        if (callback) callback(undefined, res)
    }).catch(err => {
        if (callback) callback(err, undefined);

    });
}

controller.sendVerificationEmail =  (email, token, callback)=> {

    let options = {
        to: email,
        subject: "[" + HACKATHON_NAME + "] - Verify your email"
    };

    let locals = {
        verifyUrl: ROOT_URL + '/verify/' + token
    };

    sendOne('email-verify', options, locals,  (err, info) =>{
        if (err) console.log(err);

        if (info) console.log(info.message);

        if (callback) callback(err, info);
    });

};

controller.sendPasswordResetEmail =  (email, token, callback) =>{

    let options = {
        to: email,
        subject: "[" + HACKATHON_NAME + "] - Password reset requested!"
    };

    let locals = {
        title: 'Password Reset Request',
        subtitle: '',
        description: 'Somebody (hopefully you!) has requested that your password be reset. If ' +
            'this was not you, feel free to disregard this email. This link will expire in one hour.',
        actionUrl: ROOT_URL + '/reset/' + token,
        actionName: "Reset Password"
    };

    sendOne('email-link-action', options, locals,  (err, info)=> {
        if (err) console.log(err);

        if (info) console.log(info.message);

        if (callback) callback(err, info);

    });

};

controller.sendPasswordChangedEmail =  (email, callback)=> {

    let options = {
        to: email,
        subject: "[" + HACKATHON_NAME + "] - Your password has been changed!"
    };

    let locals = {
        title: 'Password Updated',
        body: 'Somebody (hopefully you!) has successfully changed your password.',
    };

    sendOne('email-basic', options, locals,  (err, info)=> {
        if (err) console.log(err);

        if (info) console.log(info.message);

        if (callback) callback(err, info);

    });
};

module.exports = controller;
