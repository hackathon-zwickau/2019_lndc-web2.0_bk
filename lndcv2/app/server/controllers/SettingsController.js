let Settings = require('../models/Settings'),
    SettingsController = {};

SettingsController.updateField = (field, value, callback) => {
    let update = {};
    update[field] = value;
    Settings
        .findOneAndUpdate({}, {
            $set: update
        }, {new: true}, callback);
};

SettingsController.updateWhitelistedEmails = (emails, callback) => {
    Settings
        .findOneAndUpdate({}, {
            $set: {
                whitelistedEmails: emails
            }
        }, {new: true})
        .select('whitelistedEmails')
        .exec(callback);
};

SettingsController.getWhitelistedEmails = (callback) => {
    Settings.getWhitelistedEmails(callback);
};

SettingsController.updateRegistrationTimes = (open, close, callback) => {
    let updatedTimes = {};

    if (close <= open) {
        return callback({
            message: "Registration cannot close before or at exactly the same time it opens."
        });
    }

    if (open) updatedTimes.timeOpen = open;

    if (close) updatedTimes.timeClose = close;

    Settings
        .findOneAndUpdate({}, {
            $set: updatedTimes
        }, {new: true}, callback);
};

SettingsController.getRegistrationTimes = (callback) => {
    Settings.getRegistrationTimes(callback);
};

SettingsController.getPublicSettings = (callback) => {
    Settings.getPublicSettings(callback);
};

module.exports = SettingsController;