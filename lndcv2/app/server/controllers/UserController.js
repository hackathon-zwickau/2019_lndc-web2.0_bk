let _ = require('underscore'),
    User = require('../models/User'),
    Settings = require('../models/Settings'),
    Mailer = require('../services/email'),
    Stats = require('../services/stats'),
    validator = require('validator'),
    moment = require('moment'),
    UserController = {},
    maxTeamSize = process.env.TEAM_MAX_SIZE || 4,
    endsWith = (s, test) => {
        return test.indexOf(s, test.length - s.length) !== -1;
    },
    canRegister = (email, password, callback) => {
        if (!password || password.length < 6) return callback({message: "Password must be 6 or more characters."}, false);

        Settings.getRegistrationTimes((err, times) => {
            if (err) callback(err);

            let now = Date.now();

            if (now < times.timeOpen) {
                return callback({
                    message: "Registration opens in " + moment(times.timeOpen).fromNow() + "!"
                });
            }
            if (now > times.timeClose) {
                return callback({
                    message: "Sorry, registration is closed."
                });
            }

            Settings.getWhitelistedEmails((err, emails) => {
                if (err || !emails) return callback(err);

                console.log('#### emails: ', emails);

                for (let i = 0; i < emails.length; i++) {
                    if (validator.isEmail(email) && endsWith(emails[i], email)) return callback(null, true);
                }

                return callback({
                    message: "Not a valid educational email."
                }, false);
            });

        });
    };

UserController.loginWithToken = (token, callback) => {
    User.getByToken(token, (err, user) => {
        return callback(err, token, user);
    });
};

UserController.loginWithPassword = (email, password, callback) => {
    if (!password || password.length === 0) {
        return callback({
            message: 'Please enter a password'
        });
    }
    if (!validator.isEmail(email)) {
        return callback({
            message: 'Invalid email'
        });
    }
    User
        .findOneByEmail(email)
        .select('+password')
        .exec((err, user) => {
            if (err) return callback(err);

            if (!user) {
                return callback({
                    message: "We couldn't find you!"
                });
            }
            if (!user.checkPassword(password)) {
                return callback({
                    message: "That's not the right password."
                });
            }

            let token = user.generateAuthToken(),
                u = user.toJSON();
            delete u.password;

            return callback(null, token, u);
        });
};

UserController.createUser = (email, password, callback) => {

    if (typeof email !== "string") {
        return callback({
            message: "Email must be a string."
        });
    }

    email = email.toLowerCase();

    canRegister(email, password, (err, valid) => {
        if (err || !valid) return callback(err);

        let u = new User();
        u.email = email;
        u.password = User.generateHash(password);
        u.save((err) => {
            if (err) {
                if (err.name === 'MongoError' && (err.code === 11000 || err.code === 11001)) {
                    return callback({
                        message: 'An account for this email already exists.'
                    });
                }

                return callback(err);
            } else {
                let token = u.generateAuthToken(),
                    verificationToken = u.generateEmailVerificationToken();
                Mailer.sendVerificationEmail(email, verificationToken);

                return callback(
                    null,
                    {
                        token: token,
                        user: u
                    }
                );
            }
        });
    });
};

UserController.getByToken = (token, callback) => {
    User.getByToken(token, callback);
};
UserController.getAll = (callback) => {
    User.find({}, callback);
};

UserController.getPage = (query, callback) => {
    let page = query.page,
        size = parseInt(query.size),
        searchText = query.text,
        findQuery = {};
    if (searchText.length > 0) {
        let queries = [],
            re = new RegExp(searchText, 'i');
        queries.push({email: re});
        queries.push({'profile.name': re});
        queries.push({'teamCode': re});
        findQuery.$or = queries;
    }
    User
        .find(findQuery)
        .sort({
            'profile.name': 'asc'
        })
        .select('+status.admittedBy')
        .skip(page * size)
        .limit(size)
        .exec((err, users) => {
            if (err || !users) return callback(err);

            User.count(findQuery).exec((err, count) => {
                if (err) return callback(err);
                return callback(null, {
                    users: users,
                    page: page,
                    size: size,
                    totalPages: Math.ceil(count / size)
                });
            });

        });
};

UserController.getById = (id, callback) => {
    User.findById(id).exec(callback);
};

UserController.updateProfileById = (id, profile, callback) => {
    User.validateProfile(profile, (err) => {

        if (err) return callback({message: 'invalid profile'});

        Settings.getRegistrationTimes((err, times) => {
            if (err) callback(err);

            let now = Date.now();

            if (now < times.timeOpen) {
                return callback({
                    message: "Registration opens in " + moment(times.timeOpen).fromNow() + "!"
                });
            }

            if (now > times.timeClose) {
                return callback({
                    message: "Sorry, registration is closed."
                });
            }
        });

        User.findOneAndUpdate({
                _id: id,
                verified: true
            },
            {
                $set: {
                    'lastUpdated': Date.now(),
                    'profile': profile,
                    'status.completedProfile': true
                }
            },
            {
                new: true
            },
            callback);

    });
};

UserController.updateConfirmationById = (id, confirmation, callback) => {

    User.findById(id).exec((err, user) => {

        if (err || !user) return callback(err);

        if (Date.now() >= user.status.confirmBy && !user.status.confirmed) {
            return callback({
                message: "You've missed the confirmation deadline."
            });
        }

        User.findOneAndUpdate({
                '_id': id,
                'verified': true,
                'status.admitted': true,
                'status.declined': {$ne: true}
            },
            {
                $set: {
                    'lastUpdated': Date.now(),
                    'confirmation': confirmation,
                    'status.confirmed': true,
                }
            }, {
                new: true
            },
            callback);
    });
};

UserController.declineById = (id, callback) => {
    User.findOneAndUpdate({
            '_id': id,
            'verified': true,
            'status.admitted': true,
            'status.declined': false
        },
        {
            $set: {
                'lastUpdated': Date.now(),
                'status.confirmed': false,
                'status.declined': true
            }
        }, {
            new: true
        },
        callback);
};

UserController.verifyByToken = (token, callback) => {
    User.verifyEmailVerificationToken(token, (err, email) => {
        User.findOneAndUpdate({
                email: email.toLowerCase()
            }, {
                $set: {
                    'verified': true
                }
            }, {
                new: true
            },
            callback);
    });
};
UserController.getTeammates = (id, callback) => {
    User.findById(id).exec((err, user) => {
        if (err || !user) return callback(err, user);

        let code = user.teamCode;

        if (!code) {
            return callback({
                message: "You're not on a team."
            });
        }

        User
            .find({
                teamCode: code
            })
            .select('profile.name')
            .exec(callback);
    });
};

UserController.createOrJoinTeam = (id, code, callback) => {
    if (!code) {
        return callback({
            message: "Please enter a team name."
        });
    }
    if (typeof code !== 'string') {
        return callback({
            message: "Get outta here, punk!"
        });
    }
    User.find({
        teamCode: code
    })
        .select('profile.name')
        .exec((err, users) => {
            if (users.length >= maxTeamSize) {
                return callback({
                    message: "Team is full."
                });
            }

            User.findOneAndUpdate({
                    _id: id,
                    verified: true
                }, {
                    $set: {
                        teamCode: code
                    }
                }, {
                    new: true
                },
                callback);

        });
};

UserController.leaveTeam = (id, callback) => {
    User.findOneAndUpdate({
            _id: id
        }, {
            $set: {
                teamCode: null
            }
        }, {
            new: true
        },
        callback);
};

UserController.sendVerificationEmailById = (id, callback) => {
    User.findOne(
        {
            _id: id,
            verified: false
        },
        (err, user) => {
            if (err || !user) {
                return callback(err);
            }
            let token = user.generateEmailVerificationToken();
            Mailer.sendVerificationEmail(user.email, token);
            return callback(err, user);
        });
};

UserController.sendPasswordResetEmail = (email, callback) => {
    User
        .findOneByEmail(email)
        .exec((err, user) => {
            if (err || !user) {
                return callback(err);
            }

            let token = user.generateTempAuthToken();
            Mailer.sendPasswordResetEmail(email, token, callback);
        });
};

UserController.changePassword = (id, oldPassword, newPassword, callback) => {
    if (!id || !oldPassword || !newPassword) {
        return callback({
            message: 'Bad arguments.'
        });
    }

    User
        .findById(id)
        .select('password')
        .exec((err, user) => {
            if (user.checkPassword(oldPassword)) {
                User.findOneAndUpdate({
                        _id: id
                    }, {
                        $set: {
                            password: User.generateHash(newPassword)
                        }
                    }, {
                        new: true
                    },
                    callback);
            } else {
                return callback({
                    message: 'Incorrect password'
                });
            }
        });
};

UserController.resetPassword = (token, password, callback) => {
    if (!password || !token) {
        return callback({
            message: 'Bad arguments'
        });
    }

    if (password.length < 6) {
        return callback({
            message: 'Password must be 6 or more characters.'
        });
    }

    User.verifyTempAuthToken(token, (err, id) => {

        if (err || !id) return callback(err);

        User
            .findOneAndUpdate({
                _id: id
            }, {
                $set: {
                    password: User.generateHash(password)
                }
            }, (err, user) => {
                if (err || !user) return callback(err);

                Mailer.sendPasswordChangedEmail(user.email);
                return callback(null, {
                    message: 'Password successfully reset!'
                });
            });
    });
};

UserController.admitUser = (id, user, callback) => {
    Settings.getRegistrationTimes((err, times) => {
        User
            .findOneAndUpdate({
                    _id: id,
                    verified: true
                }, {
                    $set: {
                        'status.admitted': true,
                        'status.admittedBy': user.email,
                        'status.confirmBy': times.timeConfirm
                    }
                }, {
                    new: true
                },
                callback);
    });
};

UserController.checkInById = (id, user, callback) => {
    User.findOneAndUpdate({
            _id: id,
            verified: true
        }, {
            $set: {
                'status.checkedIn': true,
                'status.checkInTime': Date.now()
            }
        }, {
            new: true
        },
        callback);
};

UserController.checkOutById = (id, user, callback) => {
    User.findOneAndUpdate({
            _id: id,
            verified: true
        }, {
            $set: {
                'status.checkedIn': false
            }
        }, {
            new: true
        },
        callback);
};

UserController.makeAdminById = (id, user, callback) => {
    User.findOneAndUpdate({
            _id: id,
            verified: true
        }, {
            $set: {
                'admin': true
            }
        }, {
            new: true
        },
        callback);
};

UserController.removeAdminById = (id, user, callback) => {
    User.findOneAndUpdate({
            _id: id,
            verified: true
        }, {
            $set: {
                'admin': false
            }
        }, {
            new: true
        },
        callback);
};

UserController.getStats = (callback) => {
    return callback(null, Stats.getUserStats());
};

module.exports = UserController;
