let mongoose = require('mongoose'),
 schema = new mongoose.Schema({
  status: String,
  timeOpen: {
    type: Number,
    default: 0
  },
  timeClose: {
    type: Number,
    default: Date.now() + 31104000000
  },
  timeConfirm: {
    type: Number,
    default: 604800000
  },
  whitelistedEmails: {
    type: [String],
    select: false,
    default: ['.edu'],
  },
  waitlistText: {
    type: String
  },
  acceptanceText: {
    type: String,
  },
  confirmationText: {
    type: String
  },
  allowMinors: {
    type: Boolean
  }
});

schema.statics.getWhitelistedEmails = function(callback){
  this
    .findOne({})
    .select('whitelistedEmails')
    .exec((err, settings)=>{return callback(err, settings.whitelistedEmails);});
};

schema.statics.getRegistrationTimes = (callback)=>{
  this
    .findOne({})
    .select('timeOpen timeClose timeConfirm')
    .exec((err, settings)=>{
      callback(err, {
        timeOpen: settings.timeOpen,
        timeClose: settings.timeClose,
        timeConfirm: settings.timeConfirm
      });
    });
};

schema.statics.getPublicSettings = (callback)=>{
  this.findOne({}).exec(callback);
};
module.exports = mongoose.model('Settings', schema);
