let User = require('./models/User');
module.exports = (app) => {
    app.get('/', (req, res) => {
        res.sendfile('./app/client/index.html');
    });
    app.get('*', (req, res) => {
        res.sendfile('./app/client/index.html');
    });
};
