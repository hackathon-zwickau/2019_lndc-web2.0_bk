require('dotenv').load({silent: true});

let express = require('express'),
    bodyParser = require('body-parser'),
    methodOverride = require('method-override'),
    morgan = require('morgan'),
    mongoose = require('mongoose'),
    port = process.env.PORT || 3000,
    database = process.env.DATABASE || process.env.MONGODB_URI || "mongodb://localhost:27017",
    settingsConfig = require('./config/settings'),
    adminConfig = require('./config/admin'),
    app = express();

mongoose.connect(database);

app.use(morgan('dev'));

app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.use(methodOverride());

app.use(express.static(__dirname + '/app/client'));

let apiRouter = express.Router();
require('./app/server/routes/api')(apiRouter);
app.use('/api', apiRouter);

let authRouter = express.Router();
require('./app/server/routes/auth')(authRouter);
app.use('/auth', authRouter);

require('./app/server/routes')(app);

app.listen(port);
