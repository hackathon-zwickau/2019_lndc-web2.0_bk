require('dotenv').config();

let gulp = require('gulp'),
    sass = require('gulp-sass'),
    browserify = require('browserify'),
    browserifyNgAnnotate = require('browserify-ngannotate'),
    buffer = require('gulp-buffer'),
    cleanCss = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    source = require('vinyl-source-stream'),
    sourcemaps = require('gulp-sourcemaps'),
    uglify = require('gulp-uglify'),
    ngAnnotate = require('gulp-ng-annotate'),
    environment = process.env.NODE_ENV,
    nodemon = require('gulp-nodemon');

function swallowError(error) {
    //If you want details of the error in the console
    console.log(error.toString());
    this.emit('end');
}

gulp.task('default',  ()=> {
    console.log('yo. use gulp watch or something');
});

gulp.task('js',  () =>{
    let b = browserify({
        entries: 'app/client/src/app.js',
        debug: environment === "dev",
        transform: [browserifyNgAnnotate]
    });

    // transform streaming contents into buffer contents (because gulp-sourcemaps does not support streaming contents)
    b.bundle()
        .pipe(source('app.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(ngAnnotate())
        .on('error', swallowError)
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('app/client/build'));
});

gulp.task('sass',  () =>{
    gulp.src('app/client/stylesheets/site.scss')
        .pipe(sass())
        .on('error', sass.logError)
        .pipe(cleanCss())
        .pipe(gulp.dest('app/client/build'));
});

gulp.task('build', ['js', 'sass'],  () =>{
});

gulp.task('watch', ['js', 'sass'],  ()=> {
    gulp.watch('app/client/src/**/*.js', ['js']);
    gulp.watch('app/client/views/**/*.js', ['js']);
    gulp.watch('app/client/stylesheets/**/*.scss', ['sass']);
});

gulp.task('server', ['watch'],  () =>{
    nodemon({
        script: 'app.js',
        env: {'NODE_ENV': process.env.NODE_ENV || 'DEV'},
        watch: [
            'app/server'
        ]
    });
});
