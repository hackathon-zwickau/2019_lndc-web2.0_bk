require('dotenv').load();
let mongoose = require('mongoose');
let database = process.env.DATABASE || {url: "mongodb://localhost:27017"};
let jwt = require('jsonwebtoken');
mongoose.connect(database.url);

let User = require('../app/server/models/User');
let email = 'hacker@fh-zwickau.de';

User.findOne({
    email: email
}, function (err, user) {
    console.log(user.generateEmailVerificationToken());
    console.log(user.generateAuthToken());
    let temp = user.generateTempAuthToken();
    console.log(temp);
    console.log(jwt.verify(temp, process.env.JWT_SECRET));
});