require('dotenv').load();

let mongoose = require('mongoose'),
    database = process.env.DATABASE || "mongodb://localhost:27017",
    jwt = require('jsonwebtoken');

mongoose.connect(database);

let UserController = require('../app/server/controllers/UserController'),
    user = {email: process.env.ADMIN_EMAIL},
    userArray = require('fs').readFileSync('accepted.txt').toString().split('\n'),
    count = 0;

userArray.forEach( (id)=> {
    UserController.admitUser(id, user,  () =>{
        count += 1;
        if (count === userArray.length) {
            console.log("Done");
        }
    });
});
